var defer = require('./helpers/defer')
var noOp = function(){}

var PENDING = 0
var FULFILLED = 1
var REJECTED = 2
var HARDREJECTED = 3
var THENABLE_ERROR = {value: undefined}

//
// Promise Constructor
//

function Pop (executor) {
  var self = this
  self._pending = []
  self._pendingProxy = undefined
  self._state = PENDING
  self._onFulfilled = undefined
  self._onRejected = undefined
  self._result = {value: undefined}

  if (typeof executor !== 'function') { return }

  executor(function(value) {
    resolver(self, value)
  }, function(reason) {
    reject(self, reason)
  })
}

//
// Promise State Transitions
//

function tryTransition (self, promise, fn) {
  defer(function () {
    tryCatchHandlePromise(fn, self, promise)
  })
}

function pending (self, promise) {
  if (self._state === FULFILLED) {
    tryTransition(self, promise, handleFulfilled)
  } else if (self._state === REJECTED) {
    tryTransition(self, promise, handleRejected)
  } else if (self._state === HARDREJECTED) {
    tryTransition(self, promise, handleHardRejected)
  } else if (promise._state === PENDING) {
    self._pending.push(promise)
  }
  return promise
}

function pendingResolve (self, promise) {
  if (self._state === FULFILLED) {
    promise.onFulfilled(self._result.value)
  } else if (self._state === REJECTED) {
    promise.onRejected(self._result.value)
  }
}

function fulfill (promise, value) {
  if (promise._state === PENDING) {
    promise._state = FULFILLED
    promise._result.value = value
    applyPending(promise)

    if (promise._pendingProxy) {
      applyPendingProxy(promise)
    }
  }
}

function reject (promise, value) {
  if (promise._state === PENDING) {
    promise._state = REJECTED
    promise._result.value = value
    applyPending(promise)

    if (promise._pendingProxy) {
      applyPendingProxy(promise)
    }
  }
}

function hardReject (promise, value) {
  if (promise._state === PENDING) {
    promise._state = HARDREJECTED
    promise._result.value = value
    applyPending(promise)

    if (promise._pendingProxy) {
      applyPendingProxy(promise)
    }
  }
}

function resolver (promise, value) {
  var objectOrFunction = (typeof value === 'function' ||
                         (typeof value === 'object' && value !== null))
  if (promise === value) {
    hardReject(promise, new TypeError)
  } else if (objectOrFunction) {
    handleMaybeThenable(promise, value)
  } else {
    fulfill(promise, value)
  }
}

function handleMaybeThenable (promise, maybeThenable) {
  if (maybeThenable.constructor === promise.constructor) {
    handleOwnThenable(promise, maybeThenable)
    return
  }

  var thenOrError = getThen(maybeThenable)

  if (thenOrError === THENABLE_ERROR) {
    reject(promise, thenOrError.value)
  } else if (typeof thenOrError !== 'function') {
    fulfill(promise, maybeThenable)
  } else {
    handleThenable(promise, maybeThenable, thenOrError)
  }
}

function handleOwnThenable (promise, ownThenable) {
  if (ownThenable._state === PENDING) {
    ownThenable.then(function(data) { resolver(promise, data) },
                     function(data) { reject(promise, data)  })
  } else if (ownThenable._state === FULFILLED) {
    fulfill(promise, ownThenable._result.value)
  } else if (ownThenable._state === REJECTED) {
    reject(promise, ownThenable._result.value)
  } else if (ownThenable._state === HARDREJECTED) {
    hardReject(promise, ownThenable._result.value)
  }
}

function handleThenable (promise, thenable, then) {
  var sealed = false

  var onFulfilled = function (value) {
    if (sealed) { return }
    sealed = true

    if (thenable !== value) {
      resolver(promise, value)
      return
    }

    fulfill(promise, value)
  }

  var onRejected = function (reason) {
    if (sealed) { return }
    sealed = true
    reject(promise, reason)
  }

  var error = tryThenable(then, thenable, onFulfilled, onRejected)
  if (!sealed && error) {
    reject(promise, error)
  }
}

function tryThenable (then, thenable, onFulfilled, onRejected) {
  try { then.call(thenable, onFulfilled, onRejected) }
  catch (e) { return e }
}

function getThen (thenable) {
  try { return thenable.then }
  catch (e) {
    THENABLE_ERROR.value = e
    return THENABLE_ERROR
  }
}

function tryCatchHandlePromise (fn, promise, operation) {
  try { fn(promise, operation) }
  catch (error) { reject(operation, error) }
}

function applyPending (promise) {
  var size = promise._pending.length
  if (size > 0) {
    for (var i = 0; i < size; i += 1) {
      pending(promise, promise._pending[i])
    }
    promise._pending.length = 0
  }
}

function applyPendingProxy (promise) {
  var size = promise._pendingProxy.length
  if (size > 0) {
    for (var i = 0; i < size; i++) {
      pendingResolve(promise, promise._pendingProxy[i])
    }
    promise._pending.length = 0
  }
}

//
// Promise State Handlers
//

function handleHardRejected (promise, operation) {
  var onRejected = operation._onRejected

  if (typeof onRejected === 'function') {
    reject(operation, onRejected(promise._result.value))
  } else {
    reject(operation, promise._result.value)
  }
}

function handleFulfilled (promise, operation) {
  var onFulfilled = operation._onFulfilled

  if (typeof onFulfilled === 'function') {
    resolver(operation, onFulfilled(promise._result.value))
  } else {
    resolver(operation, promise._result.value)
  }
}

function handleRejected (promise, operation) {
  var onRejected = operation._onRejected

  if (typeof onRejected === 'function') {
    resolver(operation, onRejected(promise._result.value))
  } else {
    reject(operation, promise._result.value)
  }
}

//
// Promise API
//

Pop.prototype.then = function (onFulfilled, onRejected) {
  var self = this
  var promise = new Pop()
  promise._onFulfilled = onFulfilled
  promise._onRejected = onRejected
  return pending(self, promise)
}

Pop.prototype.catch = function (onRejected) {
  return this.then(null, onRejected)
}

Pop.prototype._proxy = function (operation) {
  var self = this

  if(!self._pendingProxy) {
    self._pendingProxy = []
  }

  self._pendingProxy.push(operation)
}

Pop.all = function (promises) {
  if(promises.length === 0) {
    return Pop.resolve(promises)
  }
  return new PromiseArray(promises)._promise
}

Pop.race = function() {}

Pop.resolve = function (value) {
  var promise = new Pop()
  resolver(promise, value)
  return promise
}

Pop.reject = function (value) {
  var promise = new Pop()
  reject(promise, value)
  return promise
}

Pop.promisify = function (nodeFn) {
  return function () {
    var self = this
    var length = arguments.length
    var args = new Array(length + 1)

    for (var i = 1; i < length; i += 1) {
      args[i] = arguments[i]
    }

    return new Pop(function (resolve, reject) {
      args[l] = function(err, val) {
        if (err) { reject(err) }
        else { resolve(val) }
      }

      nodeFn.apply(self, args)
    })
  }
}

function PromiseArray (promises) {
  var self = this
  self._values = promises
  self._results = []
  self._remaining = promises.length
  self._pos = 0
  self._promise = new Pop()
  self.init()
}

PromiseArray.prototype._fulfill = function (data) {
  var self = this
  self._remaining -= 1
  self._results[self._pos++] = data

  if(self._remaining === 0) {
    fulfill(self._promise, self._results)
  }
}

PromiseArray.prototype.init = function () {
  var self = this
  var remaining = self._remaining
  var promise

  for (var i = 0; i < remaining; i += 1) {
    promise = self._values[i]

    if (promise._state === FULFILLED) {
      self._fulfill(promise._result.value)
    } else if (promise._state === REJECTED) {
      reject(self._promise, promise._result.value)
    } else if (promise._state === HARDREJECTED) {
      hardReject(self._promise, promise._result.value)
    } else {
      promise._proxy({
        onFulfilled: function (data) {
          self._fulfill(data)
        },
        onRejected: function (reason) {
          reject(self._promise, reason)
        }
      })
    }
  }
}

module.exports = Pop
