var Pop = require('../index')

module.exports = {
  resolved: Pop.resolve,
  rejected: Pop.reject,
  deferred: function() {
    var resolve, reject;
    var promise = new Pop(function() {
      resolve = arguments[0];
      reject = arguments[1];
    });
    return {
      resolve: resolve,
      reject: reject,
      promise: promise
    };
  }
}
