function deferMethod() {
  var method = setImmediate || process.nextTick || setTimeout
  return method
}

module.exports = deferMethod()
